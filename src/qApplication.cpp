/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#include <iostream>
#include <string>
#include <memory>

#include <qApplication.hpp>

constexpr sf::Uint32 const kWndWidth{800}, kWndHeight{600};
constexpr sf::Uint32 const kOpenGLMajorVersion = 4;
constexpr sf::Uint32 const kOpenGLMinorVersion = 0;

Q_NK_BEGIN

const float64 Application::kPlayerSpeed = 100.f;
const sf::Time Application::kTimePerFrame = sf::seconds(1.f/60.f);

enum VSync : bool { Enabled = true, Disable = false };

Application::Application(std::string app_name)
    : mAppName{app_name}
    , mPlayer{}
    , mTexture{} 
    , mFont{}
    , mUtil{Utility::getInstance()}
    , mStatisticsText{}
    , mStatisticsUpdateTime{}
    , mStatisticsNumFrames{}
    , mIsMovingUp{false}
    , mIsMovingDown{false}
    , mIsMovingRight{false}
    , mIsMovingLeft{false}
{
    std::cout << "Current version: " <<
        NIGHT_KILLER_VERSION << std::endl; 
    std::cout << "Current code name: " <<
        NIGHT_KILLER_CODE_NAME << std::endl;

    auto currentAppDir = mUtil->getMainAppDir();

    // OpenGL context current version
    sf::ContextSettings contextSettings{0, 0,
        kOpenGLMajorVersion, kOpenGLMinorVersion};

    mWindow.create(
        sf::VideoMode{kWndWidth, kWndHeight}, 
        mAppName, sf::Style::Close,
        contextSettings);

    try {
        mTexture.load(Textures::ID::Entities, *currentAppDir +
                "/media/textures/Eagle.png");
        mAircraft = mTexture
            .get(Textures::ID::Entities)
            .copyToImage();
    }
    catch (std::runtime_error& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    sf::Vector2u windowSize = mWindow.getSize();
    sf::Vector2u airshapeSize = mAircraft.getSize();

    mPlayer.setTexture(mTexture.get(Textures::ID::Entities));
    mPlayer.setPosition(windowSize.x / 2 - airshapeSize.x,
                        windowSize.y / 2 - airshapeSize.y);
    try
    {
        mFont.load(Fonts::ID::Main, 
                *currentAppDir + "/media/Sansation.ttf");
    }
    catch (std::runtime_error& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    mStatisticsText.setFont(mFont.get(Fonts::ID::Main));
    mStatisticsText.setPosition(5.f, 5.f);
    mStatisticsText.setCharacterSize(10);
}

void Application::run()
{
    sf::Clock clock;
    sf::Time time_since_last_update = sf::Time::Zero;

    while ( mWindow.isOpen() )
    {
        sf::Time elapsed_time = clock.restart();
        time_since_last_update += elapsed_time;
        
        while ( time_since_last_update > kTimePerFrame )
        {
            time_since_last_update -= kTimePerFrame;

            processEvents();
            update(kTimePerFrame);
        }

        updateStatistics(elapsed_time);
        render();
    }
}

void Application::processEvents()
{
    sf::Event event;
    while ( mWindow.pollEvent(event) )
    {
        switch ( event.type )
        {
            case sf::Event::KeyPressed:
                handlePlayerInput(event.key.code, true);
                break;
            case sf::Event::KeyReleased:
                handlePlayerInput(event.key.code, false);
                break;
            case sf::Event::Closed:
                mWindow.close();
                break;
            default:
                break;
        }
    }
}

void Application::update(sf::Time elapsed_time)
{
    sf::Vector2f movement(0.f, 0.f);
    if ( mIsMovingUp )
        movement.y -= kPlayerSpeed;
    if ( mIsMovingDown )
        movement.y += kPlayerSpeed;
    if ( mIsMovingLeft )
        movement.x -= kPlayerSpeed;
    if ( mIsMovingRight )
        movement.x += kPlayerSpeed;

    mPlayer.move(movement * elapsed_time.asSeconds());
}

void Application::render()
{
    sf::Color jellyBean(37, 116, 169);
    mWindow.clear(jellyBean);
    mWindow.setVerticalSyncEnabled(VSync::Enabled);
    mWindow.draw(mPlayer);
    mWindow.draw(mStatisticsText);
    mWindow.display();
}

void Application::updateStatistics(sf::Time elapsed_time)
{
    mStatisticsUpdateTime += elapsed_time;
    mStatisticsNumFrames += 1;

    auto util = Utility::getInstance();

    if ( mStatisticsUpdateTime >= sf::seconds(1.0f) )
    {
        mStatisticsText.setString("FPS: "
                + util->toString(mStatisticsNumFrames));
        mStatisticsUpdateTime -= sf::seconds(1.0f);
        mStatisticsNumFrames = 0;
    }
}

void Application::handlePlayerInput(sf::Keyboard::Key key,
        bool isPressed)
{
    switch ( key )
    {
        case sf::Keyboard::W:
            mIsMovingUp = isPressed;
            break;
        case sf::Keyboard::S:
            mIsMovingDown = isPressed; 
            break;
        case sf::Keyboard::A:
            mIsMovingLeft = isPressed;
            break;
        case sf::Keyboard::D:
            mIsMovingRight = isPressed;
            break;
        default:
            break;
    }
}

Q_NK_END

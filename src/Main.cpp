/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#include <qApplication.hpp>
#include <Helpers/qDefinesTypes.hpp>

#include <iostream>
#include <limits>

int main() 
{
    Qubeer::Application app;
    app.run();

    auto util = Qubeer::Utility::getInstance();
    std::cout << *(util->getMainAppDir()) << std::endl;

    return 0;
}

/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#include <qUtility.hpp>
#include <string>

Q_NK_BEGIN

std::unique_ptr<Utility> Utility::mInstance{nullptr};

std::unique_ptr<Utility> Utility::getInstance()
{
    if ( mInstance.get() == nullptr )
    {
        mInstance = std::unique_ptr<Utility>{new Utility};
    }
    return std::move(mInstance);
}

std::unique_ptr<std::string>
Utility::getMainAppDir() const 
{
    char cCurrentPath[FILENAME_MAX];

    if (!getCurrentDir(cCurrentPath, sizeof(cCurrentPath)) )
    {
        std::cerr << "Couldn't get current path" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    auto currentPath = std::make_unique<std::string>(cCurrentPath);
    auto n = currentPath->find(kCurrentMainAppDir);
    if (std::string::npos == n)
    {
        std::cerr << "Current path not found" << std::endl;
        exit(EXIT_FAILURE);
    }
    currentPath->erase(n + kCurrentMainAppDir.size());
    return currentPath;
}

Q_NK_END /* END NAMESPACE QUBEER */

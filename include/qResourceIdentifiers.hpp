/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_RESOURCE_IDENTIFIERS_HPP_
#define QUBEER_RESOURCE_IDENTIFIERS_HPP_

#include <qMacros.hpp>

namespace sf 
{
    class Texture;
    class Font;
    class Shader;
    class SoundBuffer;
}

Q_NK_BEGIN

namespace Textures
{
    enum class ID
    {
        Entities,
        Buttons,
    };
}

namespace Fonts
{
    enum ID
    {
        Main,
    };
}

template<class Resource, class Identifier>
class ResourceHolder;

using TextureHolder = ResourceHolder<sf::Texture, Textures::ID>;
using FontHolder = ResourceHolder<sf::Font, Fonts::ID>;

Q_NK_END /* END NAMESPACE QUBEER */

#endif /* END QUBEER_RESOURCE_IDENTIFIERS_HPP_ */

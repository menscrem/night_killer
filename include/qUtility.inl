/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#include <iostream>
#include <sstream>
#include "qMacros.hpp"

Q_NK_BEGIN

template<typename T>
std::string Utility::toString( const T& value )
{
    std::stringstream stream;
    stream << value;
    return stream.str();
}

Q_NK_END /* END NAMESPACE QUBEER */

/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_APPLICATION_HPP_
#define QUBEER_APPLICATION_HPP_

#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Config.hpp>

#include <Helpers/qSingleton.hpp>
#include <Helpers/qDefinesTypes.hpp>
#include <qResourceHolder.hpp>
#include <qResourceIdentifiers.hpp>
#include <qNightKiller.hpp>
#include <qMacros.hpp>
#include <qUtility.hpp>

Q_NK_BEGIN

class Application : private sf::NonCopyable
{
 public:
        explicit Application(std::string app_name = "NightKiller's");
        void run();

 private:
        void processEvents();
        void update(sf::Time elapsed_time);
        void render();
        
        void updateStatistics(sf::Time elapsedTime);
        void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);

 private:
        static const float64 kPlayerSpeed;
        static const sf::Time kTimePerFrame;

        std::string mAppName;

        sf::Image mAircraft;
        sf::Sprite mPlayer;
        sf::RenderWindow mWindow;

        TextureHolder mTexture;
        FontHolder mFont;
        Util mUtil;

        sf::Text mStatisticsText;
        sf::Time mStatisticsUpdateTime;
        std::size_t mStatisticsNumFrames;

        bool mIsMovingUp;
        bool mIsMovingDown;
        bool mIsMovingRight;
        bool mIsMovingLeft;
};

Q_NK_END /*  END NAMESPACE QUBEER */

#endif /* END QUBEER_APPLICATION_HPP_ */

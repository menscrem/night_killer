/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_RESOURCE_HOLDER_HPP_
#define QUBEER_RESOURCE_HOLDER_HPP_

#include <map>
#include <string>
#include <memory>
#include <stdexcept>
#include <cassert>

#include <qMacros.hpp>

Q_NK_BEGIN

template<class Resource, class Identifier>
class ResourceHolder
{
 public:
        void load(Identifier id, const std::string& filename);
        
        template<typename Parameter>
        void load(Identifier id, const std::string& filename,
                const Parameter& secondParam);

        Resource& get(Identifier id);
        constexpr const Resource get(Identifier id);

 private:
        void insertResource(Identifier id,
                std::unique_ptr<Resource> resource);
 
 private:
        std::map<Identifier, std::unique_ptr<Resource>> mResourceMap;
};

Q_NK_END /* END NAMPESPACE QUBEER */

#include <qResourceHolder.inl>
#endif /* END QUBEER_RESOURCEHOLDER_HPP_ */

/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_CONFIG_HPP_
#define QUBEER_CONFIG_HPP_

#define NIGHT_KILLER_VERSION 0x000014
#define NIGHT_KILLER_CODE_NAME "Aberg"

#endif

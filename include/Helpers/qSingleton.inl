/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

Q_NK_BEGIN

template<typename ClassType>
ClassType* Singleton<ClassType>::m_pSingletonObject = nullptr;

template<typename ClassType>
ClassType* Singleton<ClassType>::getInstance()
{
   if (m_pSingletonObject == nullptr)
   {
      m_pSingletonObject = new ClassType();
   }
   return m_pSingletonObject;
}

template<typename ClassType>
void Singleton<ClassType>::deleteSingleton()
{
   delete m_pSingletonObject;
   m_pSingletonObject = nullptr;
}

template<typename ClassType>
Singleton<ClassType>::Singleton() {}

template<typename ClassType>
Singleton<ClassType>::~Singleton() {}

Q_NK_END /* END NAMESPACE QUBEER */

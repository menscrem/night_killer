/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_HELPERS_DEFINES_TYPES_HPP_
#define QUBEER_HELPERS_DEFINES_TYPES_HPP_

#include <cstdint>

using int64 = int64_t;
using int32 = int32_t;
using int16 = int16_t;
using int8  = int8_t;

using uint64 = uint64_t;
using uint32 = uint32_t;
using uint16 = uint16_t;
using uint8  = uint8_t;

using float32  = float;
using float64  = double;
using float128 = long double;

#endif /* END QUBEER_HELPERS_DEFINES_TYPES_HPP_ */

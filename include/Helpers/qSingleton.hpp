/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_HELPERS_SINGLETON_HPP_
#define QUBERR_HELPERS_SINGLETON_HPP_

#include <qMacros.hpp>

Q_NK_BEGIN

template<typename ClassType>
class Singleton
{
 public:
      static ClassType* getIntance();
      static void deleteSingleton();

 protected:
      Singleton();
      virtual ~Singleton();

 private:
      static ClassType* m_pSingletonObject;
};

Q_NK_END /* END NAMESPACE QUBEER */

#endif /* END QUBEER_HELPERS_SINGLETON_HPP_ */

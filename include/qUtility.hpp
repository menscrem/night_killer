/*
 * Night Kill Qubeer
 * Copyright (C) 2014 Dmitry Zhirma
 * Email: menscrem@gmail.com
*/

#ifndef QUBEER_UTILITY_HPP_
#define QUBEER_UTILITY_HPP_

#include <sstream>
#include <string>
#include <memory>

#include <qMacros.hpp>

Q_NK_BEGIN

class Utility 
{
 private:
        Utility(const Utility&) = delete;
        Utility(const Utility&&) = delete;

        Utility& operator=(const Utility&) = delete;
        Utility& operator=(const Utility&&) = delete;

 protected:
        Utility() {}

 public:
        static std::unique_ptr<Utility> getInstance();

        virtual ~Utility() {}

        template<typename T>
        std::string toString(const T& value);

        std::unique_ptr<std::string>
        getMainAppDir() const;

 public:
        static std::unique_ptr<Utility> mInstance;

        #if defined(_WIN32) || defined(__WIN32__)
            #define getCurrentDir _getcwd
            const std::string FILE_SEPR{"\\"};
        #else
            #define getCurrentDir getcwd
            static const std::string FILE_SEPR{"/"};
        #endif

        const std::string kCurrentMainAppDir{
            FILE_SEPR + "night_killer" + FILE_SEPR
        };
};

using Util = std::unique_ptr<Utility>;

Q_NK_END /* END NAMESPACE QUBEER */

#include <qUtility.inl>
#endif /* END QUBEER_UTILITY_HPP_ */

Q_NK_BEGIN

template<class Resource, class Identifier>
void ResourceHolder<Resource, Identifier>::load(Identifier id,
        const std::string& filename )
{
    auto resource = std::make_unique<Resource>();

    if ( !resource->loadFromFile(filename) )
    {
        throw std::runtime_error(
            "ResourceHolder::load -Failed to load "
            + filename);
    }

    insertResource(id, std::move(resource));
}

template<class Resource, class Identifier>
template<typename Parameter>
void ResourceHolder<Resource, Identifier>::load(Identifier id,
        const std::string& filename, const Parameter& secondParam)
{
    auto resource = std::make_unique<Resource>();

    if ( !resource->loadFromFile(filename, secondParam) )
    {
        throw std::runtime_error(
            "ResourceHolder::load -Failed to load "
            + filename);
    }

    insertResource(id, std::move(resource));
}

template<class Resource, class Identifier>
Resource& ResourceHolder<Resource, Identifier>::get(Identifier id)
{
    auto found = mResourceMap.find(id);
    assert(found != mResourceMap.end());

    return *found->second;
}

template<class Resource, class Identifier>
constexpr const Resource 
ResourceHolder<Resource, Identifier>::get(Identifier id)
{
    auto found = mResourceMap.find(id);
    assert(found != mResourceMap.end());

    return *found->second;
}

template<class Resource, class Identifier>
void ResourceHolder<Resource, Identifier>::
insertResource(Identifier id,
        std::unique_ptr<Resource> resource)
{
    auto inserted = mResourceMap.insert(std::make_pair(id,
                std::move(resource) ));
    assert(inserted.second);
}

Q_NK_END /* END QUBEER_RESOURCE_HOLDER_INL_ */

# README #

This Repo contains the source code of the game Night Killer.

### What is this repository for? ###

* Version control
* Quick develop
* Avalibility

### How do I get set up? ###

* [http://www.cmake.org/install/](Install CMake build system.)
* [http://sfml-dev.org/tutorials/2.1/#getting-started](Install SFML lib.)
* [https://code.google.com/p/box2d/](Install Box2d)

### Install Unix-like OS: ###
* Clone this repository:

```
#!bash

git clone https://menscrem@bitbucket.org/menscrem/night_killer.git
```
* Move to night_killer:

```
#!bash

cd night_killer
```

* Create build dir and move to dir:

```
#!bash

mkdir build && cd build
```
* Build project:

```
#!bash

cmake ..
make
```
* Run game:

```
#!bash

./NightKiller
```

### Contribution guidelines ###

* Writing tests
* Code review
* Google Styleguide for C++( cpplint include project.)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
